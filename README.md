# 一个基于react native 的dota视频app
### Install


```
npm install

react-native run-ios

```



#### 目标：

基于react native 开发一款兼容ios和android的dota视频app。

#### 已完成（仅ios）：
视频列表、主播列表、主播详细、视频播放

### 截图

#####标题视频列表
![image](http://wx.wefi.com.cn/images/d1.jpg)
#####主播列表
![image](http://wx.wefi.com.cn/images/d2.jpg)
#####主播详细
![image](http://wx.wefi.com.cn/images/d3.jpg)
#####视频播放
![image](http://wx.wefi.com.cn/images/d4.jpg)


ui少部分模仿开眼，其他由（tingSun ）提供支持。

### React-Native Modules In Using
>  "react-native-blur": "^0.7.11",

>  "react-native-icons": "^0.7.1",

>  "react-native-lazyload": "^0.2.1",

>  "react-native-orientation": "^1.15.0",

>  "react-native-video": "^0.7.1"